import re
import pprint
import operator
import math
import unittest


class UnitTestGenericClass(unittest.TestCase):
    pass


def genericTestFunction(actual, expected, comment):
    def testing(self):
        self.assertEqual(str(actual), str(expected), msg=comment)
    return testing


def countDigits(c):
    return len(str(abs(c)))


def channelUpDown(currentChannel, nextChannel, boundary, stopChannels, channelDigits):
    diff1 = abs(nextChannel - currentChannel)
    diff2 = boundary['r'] - boundary['l'] - diff1 + 1
    lenSC = len(stopChannels)

    # Optimization: if (channel difference - total stop channels) is higher than digits
    # in next channel, then there is no point in removing stop channels
    if((diff1 - lenSC < channelDigits) or
       (diff2 - lenSC < channelDigits)):
        for c in stopChannels:
            if(boundary['l'] <= c <= boundary['r']):
                if ((currentChannel < c < nextChannel) or
                   (currentChannel > c > nextChannel)):
                    diff1 -= 1
                else:
                    diff2 -= 1

    return min(diff1, diff2)


def optimisedSurf(previousChannel,
                  currentChannel,
                  nextChannel,
                  boundary,
                  stopChannels,
                  ):

    # Edge Cases
    if(nextChannel == currentChannel or
       nextChannel in stopChannels):
        return(0)

    # Check Back channel
    if(nextChannel == previousChannel):
        return(1)

    # Upper Ceiling is the digits in nextChannel
    channelDigits = countDigits(nextChannel)
    minButtons = min(channelDigits,
                     channelUpDown(currentChannel, nextChannel, boundary, stopChannels, channelDigits),
                     1 + channelUpDown(previousChannel, nextChannel, boundary, stopChannels, channelDigits))

    return minButtons


def driverFunction():
    testcasesFile = "testcases.txt"
    f = open(testcasesFile, 'r')

    # First test case, reading boundaries
    line = f.readline()
    testcase = 0
    while(not (line == '\n' or line == '')):
        testcase += 1
        boundary = {}
        boundary['l'], boundary['r'] = map(int, line.split())

        # stopChannels
        line = f.readline()
        stopChannels = set([int(c) for c in line.split()[1:]])

        # Channels to play
        line = f.readline()
        channelsToSurf = [int(c) for c in line.split()[1:]]

        sumOfButtons = countDigits(channelsToSurf[0])
        previousChannel = channelsToSurf[0]
        print "\nTest Case #%s\nBoundary:(%s,%s), StopChannels:%s, Surf:%s" % (
            testcase, boundary['l'], boundary['r'], stopChannels, channelsToSurf)
        for i in xrange(len(channelsToSurf) - 1):
            currentChannel = channelsToSurf[i]
            nextChannel = channelsToSurf[i+1]
            minButtons = optimisedSurf(previousChannel,
                                       currentChannel,
                                       nextChannel,
                                       boundary,
                                       stopChannels)
            sumOfButtons += minButtons
            print "%s -> %s in %s. ClicksSum- %s" % (currentChannel, nextChannel, minButtons, sumOfButtons)
            previousChannel = currentChannel

        # Expected result
        line = f.readline()
        expectedResult = int(line.split()[0])
        actualResult = sumOfButtons
        print "Expected Result- '%s', got '%s'" % (expectedResult, actualResult)

        # Performing Tests
        testFuncName = "testCase_%s" % (testcase)
        comment = "Test Failed: Expected value: '%s'. Actual value: '%s'" % (
            expectedResult, actualResult)
        unittestFunc = genericTestFunction(actualResult, expectedResult, comment)
        setattr(UnitTestGenericClass, testFuncName, unittestFunc)

        # Blank Line separating next test case
        line = f.readline()
        # Next test case, reading boundaries
        line = f.readline()
    print "_"*80
    unittest.main()
driverFunction()
